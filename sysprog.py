import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import intersection_funcs as intrsc
import time
start_time = time.time()

#install packages xlrd, pandas
xl = pd.ExcelFile('mcp_data_report.xls')
df1 = xl.parse('Sheet1')
#getting dates/hours in the table
dates = df1.axes[1][np.arange(1,len(df1.axes[1]), 2)]
textcolumns = df1.axes[1][np.arange(0,len(df1.axes[1]),2)]

volumes_kryss = []
volumes_kryss_ny = []
prices_kryss = []
prices_kryss_ny = []
#MCF_ut
#MCF_inn

# Henter inn endringer i forbruk (til etterspørsel), vindprod, kjernekraft og annet (til tilbud)
#--- henter forbruksdata
dfCons = pd.read_csv('C:\\Users\\rm6147\\OneDrive - BKK AS\\wattsightapi\\wapiclient\\out\\con-np-mwh_h-cet-min15-af.csv', decimal=',', delimiter=";", header=None)
cons = dfCons[1].astype('float64')

# vind og nuc
dfWind = pd.read_csv('C:\\Users\\rm6147\\OneDrive - BKK AS\\wapio\\out\\PRO_WND_H_F_np.csv', decimal=',', delimiter=";", header=6, quotechar='"')
cols = dfWind.axes[1]
wind_ec00 = dfWind.get(cols[1]).get_values()[0:48]

dfNuc = pd.read_csv('C:\\Users\\rm6147\\OneDrive - BKK AS\\wapio\\out\\PRO_NUC_H_F_np.csv', decimal=',', delimiter=";", header=6, quotechar='"')
cols = dfNuc.axes[1]
nuc_ec00 = dfNuc.get(cols[1]).get_values()[0:48]

#-- day-ahead changes
cons_da = []
wind_da = []
nuc_da = []

for i in range(len(cons)-24):
    cons_da.append(cons[i+24]-cons[i])
    wind_da.append(np.float64(wind_ec00[i+24].replace(',','.')) - np.float64(wind_ec00[i].replace(',','.')))
    nuc_da.append(np.float64(nuc_ec00[i+24]) - np.float64(nuc_ec00[i]))

# itererer på time
for t in range(len(dates)):
    colVal = df1.get(dates[t]).get_values()
    colTxt = df1.get(textcolumns[t]).get_values()
    # finner hvor buy curve starter og hvor sell curve starter for denne timen
    buy_index = 1 + np.where(colTxt == 'Buy curve')[0][0]
    sell_index = np.where(colTxt == 'Sell curve')[0][0]

    # lager liste for hvert sett med koordinater og "all value"-kolonner med _ foran
    price_buy = []
    volume_buy = _volume_buy = []
    price_sell = []
    volume_sell = _volume_sell = []

    #  leser pricenoteringer og leser volumnoteringer for "buy" og fjerner 'NaN' på endene
    price_buy = colVal[np.arange(buy_index, sell_index, 2)].astype(np.float64)
    price_buy = price_buy[~np.isnan(price_buy)]
    volume_buy = colVal[np.arange(buy_index+1, sell_index, 2)].astype(np.float64)
    volume_buy = volume_buy[~np.isnan(volume_buy)]

    vol_net_flows = colVal[4]
    vol_acc_buy = colVal[2]
    vol_acc_sell = colVal[3]

    for i in range(len(volume_buy)):
        if vol_net_flows < 0:
            _volume_buy.append(volume_buy[i] + vol_acc_buy - vol_net_flows) # + MCF_ut
        else:
            _volume_buy.append(volume_buy[i] + vol_acc_buy) # + MCF_ut
    _volume_buy = np.array(_volume_buy)

    np.where(_volume_sell == _volume_buy[0])
    price_sell

    # leser pricenoteringer og leser volumnoteringer for "sell"
    price_sell = colVal[np.arange(sell_index+1, len(colVal), 2)].astype(np.float64)
    price_sell = price_sell[~np.isnan(price_sell)]
    volume_sell = colVal[np.arange(sell_index+2, len(colVal), 2)].astype(np.float64)
    volume_sell = volume_sell[~np.isnan(volume_sell)]

    for i in range(len(volume_sell)):
        if vol_acc_buy > 0:
            _volume_sell.append(volume_sell[i] + vol_acc_sell + vol_acc_buy) #+MCF_inn
        else:
            _volume_sell.append(volume_sell[i] + vol_acc_sell) # + MCF_inn

    _volume_sell = np.array(_volume_sell)

    #find volumes in buy that are not in sell_curve
    #isin = np.isin(volume_sell, volume_buy)
    #if not np.count_nonzero(isin):
        #concatenate volumes for buy and sell
        #volume_axis = np.sort(np.array(volume_buy.tolist() + volume_sell.tolist()))
        #otherwise take out only what's unique

    #price_sell_int = np.interp(volume_axis, volume_sell, price_sell)
    #plt.plot(volume_axis, price_sell_int)

    #Finner kryss, gammel pris
    intrx_pnt = intrsc.intersection(_volume_sell, price_sell, _volume_buy, price_buy)
    x = intrx_pnt[0][0]
    volumes_kryss.append(x)
    y = intrx_pnt[1][0]
    prices_kryss.append(y)
    #plt.plot(_volume_sell, price_sell)
    #plt.plot(_volume_buy, price_buy)
    #plt.plot(x,y, 'ro')
    # plt.title(dates[t])
    # plt.xlabel('Volume, MWh')
    # plt.ylabel('Price, EUR')
    # plt.show()
    print('Likevekt ved x = ' + str(x) + ', y = ' + str(y) + '\n')

    # -- oppdaterer etterspørsel og tilbud
    _volume_buy += cons_da[t]
    _volume_sell += (wind_da[t] + nuc_da[t])

    # Kryss, ny pris
    intrx_pnt = intrsc.intersection(_volume_sell, price_sell, _volume_buy, price_buy)
    x = intrx_pnt[0][0]
    volumes_kryss_ny.append(x)
    y = intrx_pnt[1][0]
    prices_kryss_ny.append(y)
    # plt.plot(_volume_sell, price_sell)
    # plt.plot(_volume_buy, price_buy)
    # plt.plot(x,y, 'ro')
    # plt.title('Neste dag, time ' + str(t))
    # plt.xlabel('Volume, MWh')
    # plt.ylabel('Price, EUR')
    # plt.show()
    print('Likevekt ved x = ' + str(x) + ', y = ' + str(y) + '\n')

# Gammel pris
prices_day_old = pd.Series(prices_kryss, index=dates)
price_sys_day_old = round(prices_day_old.mean(), 2)
# prices_day_old.name = "Realisert døgnpris, system = " + str(price_sys_day_old)
# prices_day_old.plot()
# plt.plot(dates, np.full(24, price_sys_day_old), '--')
# plt.title(prices_day_old.name)
# plt.xlabel("Timer")
# plt.ylabel("Systempris")
# plt.show()

# Ny pris
prices_day = pd.Series(prices_kryss_ny, index=dates)
price_sys_day = round(prices_day.mean(), 2)
price_sys_day
# prices_day.name = "Prognosert pris neste døgn, system = " + str(price_sys_day)
# prices_day.plot()
# plt.plot(dates, np.full(24, price_sys_day), '--')
# plt.title(prices_day.name)
# plt.xlabel("Timer")
# plt.ylabel("Systempris")
# plt.show()


print("--- %s seconds ---" % (time.time() - start_time))
